#!/bin/bash

directoryName="protected-data"
archiveName="protected-data.tar.gz"
encryptedArchiveName="protected-data.tar.gz.gpg"

# Delete the existing encrypted archive, if it exists
rm $encryptedArchiveName

# Tar the protected-data directory. This will overwrite if archive already exists.
tar -czvf $archiveName $directoryName 

# Encrypt the tarball. User will be prompted for an encryption password.
gpg --output $encryptedArchiveName --symmetric --cipher-algo AES256 $archiveName 

# Delete the unencrypted archive
rm $archiveName
