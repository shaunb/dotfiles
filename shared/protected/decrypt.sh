#!/bin/bash

directoryName="protected-data"
archiveName="protected-data.tar.gz"
encryptedArchiveName="protected-data.tar.gz.gpg"

# Delete the existing unencrypted directory, if it exists
rm -r $directoryName

# Decrypt
gpg --output $archiveName --decrypt $encryptedArchiveName

# Untar
tar -zxvf $archiveName

# Delete the unencrypted archive
rm $archiveName
