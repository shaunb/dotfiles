#!/bin/bash

cloneToDirectory="$HOME/git-test"
remoteList=()
repoList=()

# Remember that bash functions can only return error codes, not real values (they're 
# really commands, not functions). To simulate returning a real value, printf or echo, 
# and call the function using command substitution (allow the output of a command to 
# replace the command itself).
getRemoteUrl() {
    local remoteUrl="$1"
    local appendDotGitSuffix="$2"

    if [[ "$appendDotGitSuffix" = true ]] 
    then
        pushUrl="$remoteUrl/$repo.git"
        printf "$pushUrl"
    else
        pushUrl="$remoteUrl/$repo"
        printf "$pushUrl"
    fi
}

# Get the list of repos to clone and push remotes to add. Strip out any comments and 
# empty lines. Bash syntax explanation:
# http://unix.stackexchange.com/questions/146942/how-can-i-test-if-a-variable-is-empty-or-contains-only-spaces
# http://stackoverflow.com/questions/10929453/read-a-file-line-by-line-assigning-the-value-to-a-variable
populateLists() {    
    while read -r line; do
        if [[ "$line" == "#"* ]] || [[ -z "${line// }" ]]; then continue; fi
        repoList+=("$line")
    done < "repo-list.txt"

    while read -r line; do
        if [[ "$line" == "#"* ]] || [[ -z "${line// }" ]]; then continue; fi
        remoteList+=("$line")
    done < "remote-list.txt"
}

run() {
    # Create the directory we'll be cloning to
    mkdir $cloneToDirectory &> /dev/null

    # Populate our remote and repo lists
    populateLists

    # Clone all repos that don't already exist, and set additional push urls
    # http://stackoverflow.com/questions/14290113/git-pushing-code-to-two-remotes
    for repo in "${repoList[@]}"
    do
        # Clone
        local pathOnDisk="$cloneToDirectory/$repo/"
        local fetchUrl=$(getRemoteUrl ${remoteList[0]})
        git clone $fetchUrl $pathOnDisk

        for remote in "${remoteList[@]}"
        do
            # Set push urls. Delete first to prevent adding duplicates.
            local pushUrl=$(getRemoteUrl $remote)
            git -C $pathOnDisk remote set-url --delete --push origin $pushUrl &> /dev/null
            git -C $pathOnDisk remote set-url --add --push origin $pushUrl
        done
    done
}

run
