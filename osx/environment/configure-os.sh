# Run commands to configure the operating system

# This will unset /usr/local/etc/gitconfig (system level), which defaults to always 
# using osxkeychain. We want to control this at the global (user) level.
git config --system --unset credential.helper
